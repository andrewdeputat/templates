package com.deputat.dadc.task.db.core;


import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Andrew on 18.09.2016.
 */
public abstract class ARealmController implements IRealmOperation, IRealmController {

    private static final long SCHEMA_VERSION = 1;

    @Override
    public Realm buildConfiguration(Context context) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context)
                .migration(new Migration())
                .schemaVersion(SCHEMA_VERSION)
                .build();

        return Realm.getInstance(realmConfiguration);
    }

    @Override
    public <T extends RealmObject> void add(final T t) {
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(t);
            }
        });

        backup();
    }

    @Override
    public <T extends RealmObject> void update(final T t) {
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(t);
            }
        });

        backup();
    }

    @Override
    public <T extends RealmObject> void delete(final T t) {
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                t.deleteFromRealm();
            }
        });

        backup();
    }

    @Override
    public <T extends RealmObject> void deleteAll(final Class<T> t) {
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(t);
            }
        });

        backup();
    }

    @Override
    public <T extends RealmObject> RealmResults<T> getAll(Class<T> t) {
        return getRealm().where(t).findAll();
    }
}
