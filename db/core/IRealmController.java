package com.deputat.dadc.task.db.core;


import android.content.Context;

import io.realm.Realm;

/**
 * Created by Andrew on 18.09.2016.
 */
interface IRealmController {
    Realm buildConfiguration(Context context);

    void closeRealm();

    Realm getRealm();

    void backup();
}
