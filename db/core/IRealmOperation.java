package com.deputat.dadc.task.db.core;

import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Andrew on 18.09.2016.
 */
interface IRealmOperation {
    <T extends RealmObject> void add(final T t);

    <T extends RealmObject> void update(final T t);

    <T extends RealmObject> void delete(final T t);

    <T extends RealmObject> void deleteAll(final Class<T> t);

    <T extends RealmObject> RealmResults<T> getAll(Class<T> t);
}
