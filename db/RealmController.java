package com.deputat.dadc.task.db;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.deputat.dadc.task.db.core.ARealmController;

import io.realm.Realm;

/**
 * Created by Andrew on 18.09.2016.
 */
public class RealmController extends ARealmController{

    private static RealmController sInstance;
    private final Realm mRealm;

    private RealmController(Application application){
        mRealm = buildConfiguration(application);
    }

    public static RealmController with(Fragment fragment) {

        if (sInstance == null) {
            sInstance = new RealmController(fragment.getActivity().getApplication());
        }
        return sInstance;
    }

    public static RealmController with(Activity activity) {

        if (sInstance == null) {
            sInstance = new RealmController(activity.getApplication());
        }
        return sInstance;
    }

    public static RealmController with(Application application) {

        if (sInstance == null) {
            sInstance = new RealmController(application);
        }
        return sInstance;
    }

    @Override
    public void closeRealm() {
        mRealm.close();
    }

    @Override
    public Realm getRealm() {
        return mRealm;
    }

    @Override
    public void backup() {
        // TODO backup;
    }
}
